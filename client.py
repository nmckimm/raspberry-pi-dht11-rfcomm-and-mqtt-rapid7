"""
COMP47520 - Project 2
Stephen Colfer
Neil McKimm

Client script for connection via Bluetooth and Wifi to capture and send data from the DHT11 
Temperature and Humidity sensor. 
The pi first establishes a Wifi connection via MQTT to the broker and topic "stephanie". This 
connection sends data taken every 10 seconds in real-time and runs for "12 hours" theoretically.
A separate connection is then established via bluetooth and data is then sent for another 
"12 hours". The connection swaps every "12 horus then" and terminates after certain amount of 
days specififed in the final loop.
 
Note: 
- May require 'python' command to run instead of 'python3'.
- Time.sleep for each function can be adjusted to changed data capture times (must match
  server script).
"""


import Adafruit_DHT as Adafruit
import time
import datetime
import bluetooth
import paho.mqtt.client as mqtt



# MQTT Info and initial connection to broker
broker_address="192.168.88.232"
client = mqtt.Client("pi")
client.connect(broker_address)

# Bluetooth Info
target_address = "B8:27:EB:09:9C:9D"
port = 3

# Sensor Info
pin = 4
sensor = Adafruit.DHT11

# Capture data from sensor
def tempCapture():
    try:
        humidity, temperature = Adafruit.read_retry(sensor, pin)
    except:
        print("unable to gain reading from sensor")

    return humidity, temperature


# Send data via MQTT for a day (12 iterations)
def hrWifi(client):
	"""
	Takes input of client connection. Captures data every 10 seconds for 12 iterations, simulating 12 hours.
	Before the last iteration a bluetooth connection is established and connection socket is returned.
	"""
    client.loop_start()
    iterations = 12

    for i in range(0,iterations):
        try:
            humidity, temperature = tempCapture()
        except:
            print("unable to gain reading from sensor")

        if humidity is not None and temperature is not None:
            s = str((datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f"), "MQTT", "Stephen", temperature, humidity)) 
            print(s)
            client.publish("stephanie", s)

        if i == (iterations-1):
            sock = bluetooth.BluetoothSocket( bluetooth.RFCOMM )
            sock.connect((target_address, port))
            print("Starting Bluetooth")

        time.sleep(9.5)
    print("Stopping MQTT")
    client.loop_stop()
    return sock


# Send data via bluetooth for a day (12 iterations)
def hrBluetooth(socket):
	"""
	Takes input of socket connection. Captures data every 10 seconds for 12 iterations, simulating 12 hours.
	Before the last iteration a wifi connection is established and client connection is returned.
	"""
    iterations = 12
    for i in range(0, iterations):
        try:
            humidity, temperature = Adafruit.read_retry(sensor, pin)
        except:
            print("unable to gain reading from sensor")


        if humidity is not None and temperature is not None:
            s = str((datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f"), "RFCOMM", "Stephen", temperature, humidity)) 
            print(s)
            socket.send(s)

        if i == iterations-1:
            client.connect(broker_address)
            client.publish("stephanie", "Stephen's data incoming!")
            print("Starting MQTT")

        time.sleep(9.5)
    print("Stopping Bluetooth")
    socket.close
    return client

# Main function - Each loop acts as full day.
days = 4
for i in range (0, days):
    sock = hrWifi(client)
    client = hrBluetooth(sock)
