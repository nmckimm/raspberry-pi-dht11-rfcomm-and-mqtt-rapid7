"""
COMP47520 - Project 2
Stephen Colfer
Neil McKimm

Paho Mosquitto client and bluetooth server script that receives data from
a bluetooth client for "twelve hours" and then swaps to receiving data
via subscribing to an mqtt topic "stephanie". The data transmitted is
humidity and temperature data.

This script logs the received data and logs its own at the same time and uploads
the data to rapid7.


Note: 
- May require 'python' command to run instead of 'python3'.
- Time.sleep for each function can be adjusted to changed data capture times (must match
  client script).
"""

import paho.mqtt.subscribe as subscribe
import paho.mqtt.client as mqtt

import Adafruit_DHT
import os
import time
from time import sleep
from datetime import datetime as dt
import bluetooth

from r7insight import R7InsightHandler
import logging


# Bluetooth Connection
def bluetoothConnection():
    
    server_sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
    port = 3
    server_sock.bind(("", port))
    server_sock.listen(3)

    client_sock, address = server_sock.accept()
    print("Accepted connection from ", address)
    return client_sock, server_sock


# Read information recieved via Bluetooth
def bluetoothReceive(client_sock,server_sock):
    """
    Takes input of client socket and server socket to read in bluetooth data from client.
    Sensor data of both client and server are then logged to R7. Runs every 10 seconds 
    for 12 iterations. 
    """
    sensor = Adafruit_DHT.DHT11
    pin = 4

    # Loop to check temperature and humidity values a certain amount of iterations
    for i in range(0, 12):

        # Client data
        clientData = client_sock.recv(2048)
        print("received [%s]" % clientData)
        clientData = clientData[1:len(clientData) - 1]
        timeCli, protocolCli, sensorCli, temperatureCli, humidityCli = clientData.split(",")

        # Server Data
        humiditySer, temperatureSer = Adafruit_DHT.read_retry(sensor, pin)

        # Log Server sensor data
        now = dt.now()
        current_date = dt.date(now)
        current_time = dt.time(now)
        log.info(
            str(now) + "\tSensorID=Neil" + "," + "Humidity={} %".format(humiditySer) + "," + "Temperature={} C".format(
                temperatureSer) + "\n")

        # Alert Server sensor data
        log.addHandler(test)
        alert = alertFunct(humiditySer, temperatureSer)
        log.warn(alert)

        # Log Client sensor data
        log.info(str(timeCli) + "\tProtocol=" + protocolCli.strip() + "," + " SensorID=Stephen" + "," + "Humidity={} %".format(
            float(humidityCli)) + "," + "Temperature={} C".format(float(temperatureCli)) + "\n")

        # Alert Client sensor data
        log.addHandler(test)
        alert2 = alertFunct(humidityCli, temperatureCli)
        log.warn(alert2)

        time.sleep(9)

    client_sock.close()
    server_sock.close()


# Creates alert messages depending on temperature and humidity values.
def alertFunct(humidity, temperature):
    tempAlert = ""
    humAlert = ""

    humidity, temperature = float(humidity), float(temperature)

    if temperature > 28:
        tempAlert += "It is very hot"
    elif 28 > temperature >= 25:
        tempAlert += "It is warm"
    elif temperature < 25:
        tempAlert += "Temperature is normal"

    if 70 > humidity > 50:
        humAlert += "humidity is high"
    elif 101 > humidity > 69:
        humAlert += "humidity is very high"

    if (len(humAlert) > 1 and len(tempAlert) > 1):
        alert = tempAlert + " and " + humAlert
    elif (len(humAlert) > 1 and len(tempAlert) < 1):
        alert = humAlert
    else:
        alert = tempAlert
    return alert


log = logging.getLogger('Pi')
log.setLevel(logging.INFO)
test = R7InsightHandler('f271c668-bd6f-4d7f-92ca-dec95300fe58', 'eu')

pin = 4
sensor = Adafruit_DHT.DHT11

i=0
j=0

# Main function - runs for 4 days
days = 4
for i in range(0, days, 1):

    day = True
    if day:

        # Initial connection via wifi
        broker_address = "192.168.88.232"
        print("creating new instance")

        client = mqtt.Client("NeilyPi")
        client.connect(broker_address)
        client.loop_start()
        client.subscribe("stephanie", qos=0)
        topics = ['#']

        # Loop for 12 iterations to capture and log sensor data via MQTT connection
        for j in range(0, 12, 1):

            message = subscribe.simple(topics, hostname=broker_address, retained=False, msg_count=1)
            print(message.topic)
            print(message.payload)
            
            clientData = message.payload
            clientData = clientData[1:len(clientData) -1]
            print(clientData)

            #Client data
            timeCli, protocolCli, sensorCli, temperatureCli, humidityCli = clientData.split(",")
            temperatureCli, humidityCli = temperatureCli.strip(), humidityCli.strip()

            #Server data
            humiditySer, temperatureSer = Adafruit_DHT.read_retry(sensor, pin)
            print("server data")

            #Log server data
            now = dt.now()
            current_date = dt.date(now)
            current_time = dt.time(now)
            print(str(now) + "\tSensorID=Neil" + "," + "Humidity={} %".format(humiditySer) + "," + "Temperature={} C".format(temperatureSer) + "\n")
            log.info(str(now) + "\tSensorID=Neil" + "," + "Humidity={} %".format(humiditySer) + "," + "Temperature={} C".format(temperatureSer) + "\n")

            #Alert server data
            log.addHandler(test)
            alert2 = alertFunct(humidityCli, temperatureCli)
            log.warn(alert2)

            #Log client sensor data
            log.info(str(timeCli) + "\tProtocol="+ protocolCli.strip() + "," +  "SensorID=Stephen" + "," + "Humidity={} %".format(humidityCli) + "," + "Temperature={} C".format(temperatureCli) + "\n") 

            # Alsert Client sensor data
            log.addHandler(test)
            alert2 = alertFunct(humidityCli.strip(), temperatureCli.strip())
            log.warn(alert2)
            
            time.sleep(5)

            # Establish BLuetooth connection
            if j == 11:
                sock,server = bluetoothConnection()
            print("end of loop")
        client.loop_stop()

    # Runs bluetooth part of day    
    bluetoothReceive(sock,server)
