# Raspberry Pi DHT11 RFCOMM And MQTT Rapid7

Simulating an edge node and gateway where two Raspberry Pis use Bluetooth LE at night and WiFi during the day to send data from client to server and one uploads the data to Rapid7 for analysis.

 Report - December 2018 
			





Team: Neil McKimm, Stephen Colfer

	
						
UCD School of Computer Science
University College Dublin 
December, 2018 




Introduction
Many IoT networks which utilise sensor data have to deal with the issue of power, as most sensors may not always be in a position for an easily accessible power source. Batteries/solar energy act as the best possible alternatives, however this can be expensive and difficult to maintain depending on the quality of the hardware. To improve efficiency on this, low bandwidth, low power consumption methods of data transfer are ideal. The scenario for this project outlines a system powered by photovoltaic cells which charge during the day but in order to last through the night, a low power communication protocol must be used. Therefore we set out to combat this with a system that changes data transfer protocol every 12 hours to save energy during the night. 

Instructions for Use
Connect the DHT11 to the correct GPIO pins for both Pi’s
Select one Pi to act as Broker 
Run broker in the background: ‘mosquitto -d’
Run server.py on Pi 1 (ensure broker address is correct).
Run client.py on Pi 2.

Server.py
Paho Mosquitto client and bluetooth server script that receives data from a bluetooth client for "12 hours" and then swaps to receiving data via subscribing to a MQTT topic "stephanie". The data transmitted is humidity and temperature data taken from the DHT11 sensor. This script logs the received data and logs its own at the same time and uploads the data to rapid7 as well as alert strings. 

Client.py
Publishes temperature and humidity data from DHT11 sensor to the MQTT broker topic “stephanie” each hour for “12 hours”. For the purposes of this project each hour is 10 seconds in real time. After 12 hours the connection is swapped and the data is then transmitted via bluetooth. This process repeats for 4 theoretical days. Data is transferred as a tuple with each attribute. The server then manipulates it into a readable alert format.

System Architecture
Sensors
The sensors used were the Adafruit DHT11 temperature and humidity sensors. Each Raspberry pi had one connected to it and the data that was sent was formatted as in the Sample Output figure shown at the end of the document.

Mosquitto
The broker was set up on the server pi. The daemon is ran in the background and a topic was set up. The server subscribes to the broker’s topic and awaits data being published to this topic. The client publishes data to this topic (“stephanie”) in the form of a topic name and a payload.


Bluetooth
The bluetooth architecture was designed very similarly to the previous part of the project. The pis were paired via bluetooth and the Server listens on port 3 using an RFCOMM interface. This allowed the Client to establish a connection and then begin transmitting the data to the Server.

For both of these cases the Server established a connection with rapid7 and published data from both the Client and the Server to a log named rpi. This logged the data and allowed us to analyse it by using Insight’s own query language - Log Entry Query Language (LEQL). 

Rationale for Design Choices
Having already successfully created a robust bluetooth network for transferring sensor data in the previous project, our main design choices for this project were based on MQTT and connection swapping implementation. Otherwise we utilised our previous bluetooth design. We chose Mosquitto and Paho as we were advised they are the most readily available Python libraries for MQTT. In terms of connection transfer we considered several different architectures, including multi-threading systems as well as pre-emptive loops. We decided for the latter as we believed this to be the simplest to implement within the time frame given and limited connection time to the router for testing and coding. 

Pre-emptive looping means that for the amount of iterations to be run, an alternative connection will be made before the final piece of data is sent to allow time for the connection to complete i.e. for 12 MQTT iterations, the bluetooth connection will be created on the 11th. Drawbacks of this include timing between the client and server however we found it to be the simplest solution logically to understand and implement. 

We also decided to transmit client data as a tuple of attribute values required (as opposed to the full alert string) to make the data values usable by the server to format or interpret if needed to compare or use in future programs.



Problems 
Router: This was an exceptionally frustrating blocker in the code design process. We tried to connect our pis to other networks such as portable hotspots on both our phones with and without different encryption algorithms but the Pis were unable to get an internet connection, therefore logging to rapid7 was impossible. The only way we could actually work was when connected to the COMP47520 router (during the lab sessions or in the hall of the computer science building). Had we not have been stuck for so long on this project just trying to connect to our Pis we would have liked to implement some more intricate code and we would have been able to provide a better and more varied log set. 

Changing seamlessly between bluetooth and wifi: seeing as we didn’t implement multi-threading, our ‘pre-emptive loop’ design could was dependent on linking the timing of the client and server scripts.

The times on the Pi. Each time the Pis were turned on the time was incorrect, hence the inaccuracy of the time in the sample output.


Improvements
Adding multithreading to ensure seamless transition between connection protocols.
Adding error handling for unsuccessful connections such as reverting to alternative connection, provided sufficient power is available.
Error handling for outlier results.
Ensuring the wifi and bluetooth connections are fully disabled to save as much power as possible.
Actually testing this as intended - using solar power. This would give us more information that we could use on how best to optimise the system and extrapolate at scale.
Optimising the server script to make better use of functions which will reduce the size of the script and hopefully reduce the running time.
Using more accurate sensors for more precise measurements
Finding a permanent fix for the time and date on the systems. After each reset the time and dates revert to a random date.
If this were to be scaled up, a new system architecture would need to be designed dependent on the location of the edge nodes and gateways. The maximum number of edge clients per broker would also need to be determined.




